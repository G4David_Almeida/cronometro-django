from django.urls import path
from .views import StartPusherView, PausePusherView, ContinuePusherView, RestartPusherView, IncludesPusherView

urlpatterns = [
    path('start/', StartPusherView.as_view(), name="start"),
    path('pause/', PausePusherView.as_view(), name="pause"),
    path('continue/', ContinuePusherView.as_view(), name="continue"),
    path('restart/', RestartPusherView.as_view(), name="restart"),
    path('includes/', IncludesPusherView.as_view(), name="includes")
]