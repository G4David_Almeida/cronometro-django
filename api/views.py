from rest_framework.views import APIView
from rest_framework.response import Response

# pusher
from .config.pusher import pusher_client

class StartPusherView(APIView):
    def post(self, request, format=None):
        pusher_client.trigger('chronometer', 'start', request.data)

        return Response({})

class PausePusherView(APIView):
    def post(self, request):
        pusher_client.trigger('chronometer', 'pause', {})

        return Response({})

class ContinuePusherView(APIView):
    def post(self, request):
        pusher_client.trigger('chronometer', 'continue', {})

        return Response({})

class RestartPusherView(APIView):
    def post(self, request):
        pusher_client.trigger('chronometer', 'restart', request.data)

        return Response({})

class IncludesPusherView(APIView):
    def post(self, request):
        pusher_client.trigger('chronometer', 'includes', request.data)

        return Response({})