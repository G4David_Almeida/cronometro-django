from django.views.generic import TemplateView

class IndexView(TemplateView):
    template_name = 'index.html'

class DisplaView(TemplateView):
    template_name = 'display.html'

class ControlsView(TemplateView):
    template_name = 'controls.html'

"""
https://dashboard.pusher.com/apps/1134233/getting_started
pusher_client.trigger('my-channel', 'my-event', {'message': 'hello world'})
"""