const requestEvent = (url, method, data) => new Promise((resolve, reject) => {
    fetch(url, {
        method: method,
        credentials: 'same-origin',
        headers: {
            "X-CSRFToken": getCookie("csrftoken"),
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(data => {resolve(data)})
        .catch(err => {reject(err)})
})