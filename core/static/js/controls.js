(function () {
    'use strict'
    const btnStart = document.querySelector('#start');
    const btnPause = document.querySelector('#pause');
    const btnContinue = document.querySelector('#continue');
    const btnRestart = document.querySelector('#restart');
    const logs = document.querySelector('.log-box');
    let actionInExecution = false;

    const includes = Array.from(document.querySelectorAll('div.includes button'));

    const state = {
        timeStartedIn: {
            hours: '00',
            minutes: '05',
            seconds: '00',
        },
        timeWithIncludes : {
            hours: '00',
            minutes: '05',
            seconds: '00',
        }
    };
    // nessas horas que sinto falta do typescript :-(
    function activateOrDeactivate (toActivate, toDeactivate) {
        toActivate.forEach(function (element) {
            element.setAttribute('style', '')
        });
        toDeactivate.forEach(function (element) {
            element.setAttribute('style', 'opacity: 0.5; pointer-events: none;');
        })
    }

    function createLog (text) {
        const p = document.createElement('p')
        p.innerHTML = text;

        logs.append(p)
        p.classList.add('show')

        setInterval(() => p.classList.remove('show'), 4000)
        setInterval(() => p.remove(), 5000)
        actionInExecution = false;
    }

    activateOrDeactivate([], [btnPause, btnContinue, btnRestart]);

    btnStart.addEventListener('click', async function () {
        if (actionInExecution) return;
        actionInExecution = true;

        // do npm também :-(
        await requestEvent('/api/start/', 'post', state)

        activateOrDeactivate([btnRestart, btnPause], [btnStart, btnContinue]);

        createLog('contador iniciado')
    });

    // do babel também
    btnPause.addEventListener('click',async function() {
        if (actionInExecution) return;
        actionInExecution = true;

        await requestEvent('/api/pause/', 'post', state)

        activateOrDeactivate([btnContinue], [btnPause])

        createLog('contador pausado')
    })

    btnContinue.addEventListener('click', async function() {
        if (actionInExecution) return;
        actionInExecution = true;

        await requestEvent('/api/continue/', 'post', state)

        activateOrDeactivate([btnPause], [btnContinue])

        createLog('contador continuado')
    })

    btnRestart.addEventListener('click', async function () {
        if (actionInExecution) return;
        actionInExecution = true;

        await requestEvent('/api/restart/', 'post', state)

        activateOrDeactivate([btnPause], [btnContinue])

        createLog('tempo reiniciado')
    })

    // mas o python tá de parabéns, não é a toa que eu ouso todo mundo falar super bem dela ;-)
    includes.forEach(function (element, index) {
        element.addEventListener('click', async function () {
            if (actionInExecution) return;
            actionInExecution = true;

            let time = parseInt(state.timeWithIncludes.minutes) + index + 1;
            if (time <= 59) {
                addTime('min', index + 1);
            } else if (parseInt(state.timeWithIncludes.hour) !== 24) {
                let exceeded = time - 60;
                addTime('hour');
                state.timeWithIncludes.minutes = '0' + exceeded;
            } else {
                state.timeWithIncludes.minutes = '59';
                state.timeWithIncludes.seconds = '59';
            }

            if (btnStart.style.length < 2) {
                state.timeStartedIn = { ...state.timeWithIncludes }
            }

            await requestEvent('/api/includes/', 'post', { state: state, plusMinutes: index + 1 })

            createLog('mais ' + (index + 1) + 'min adicionado')
        })
    })

    function addTime (propName, value=false) {
        let time = value
            ? parseInt(state.timeWithIncludes[propName]) + value
            : parseInt(state.timeWithIncludes[propName]) + 1;

        if (time < 10) {   
            state.timeWithIncludes[propName] = '0' + time;
        } else {
            state.timeWithIncludes[propName] = time;
        }
    }
})()