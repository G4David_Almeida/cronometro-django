(function (){
    'use strict'
    const hour = document.querySelector('.display .hours');
    const min = document.querySelector('.display .min');
    const sec = document.querySelector('.display .sec');

    const btnStart = document.querySelector('#start');
    const btnPause = document.querySelector('#pause');
    const btnContinue = document.querySelector('#continue');
    const btnRestart = document.querySelector('#restart');

    const includes = Array.from(document.querySelectorAll('div.includes button'));

    const states = {
        isRunning: false,
        started: false,
        originalTime: {
            hour: '',
            min: '',
            sec: '',
        },
        tiktok: null
    };

    function addTime (element, value=false) {
        let time = value
            ? parseInt(element.innerHTML) + value
            : parseInt(element.innerHTML) + 1;

        if (time < 10) {   
            element.innerHTML = '0' + time;
        } else {
            element.innerHTML = time;
        }
    }

    function removeTime (element) {
        let time = parseInt(element.innerHTML) - 1;
        if (time < 10) {   
            element.innerHTML = '0' + time;
        } else {
            element.innerHTML = time;
        }
    }

    function tiktok () {
        let hours = parseInt(hour.innerHTML);
        let minutes = parseInt(min.innerHTML);
        let seconds = parseInt(sec.innerHTML);

        if (hours === 0 && minutes === 0 && seconds === 0) {
            alert('Tempo acabou');
            clearInterval(states.tiktok);
        } else {
            if (seconds > 0) {
                removeTime(sec);
            } else if (minutes > 0) {
                removeTime(min);
                sec.innerHTML = 59;
            } else {
                removeTime(hour);
                min.innerHTML = 59;
                sec.innerHTML = 59;
            }
        }
    }

    btnStart.addEventListener('click', function () {
        if (states.started) return;
        states.isRunning = true;
        states.started = true;

        states.originalTime.hour = hour.innerHTML;
        states.originalTime.min = min.innerHTML;
        states.originalTime.sec = sec.innerHTML;
        
        states.tiktok = setInterval(tiktok, 1000);
    });

    btnPause.addEventListener('click', function() {
        if (!states.isRunning) return;
        states.isRunning = false;

        clearInterval(states.tiktok)
    })

    btnContinue.addEventListener('click', function() {
        if (states.started && states.isRunning) return;
        states.isRunning = true;
        
        states.tiktok = setInterval(tiktok, 1000);
    })

    btnRestart.addEventListener('click', function () {
        if (!states.isRunning  && !states.started) return
        states.isRunning = true
        clearInterval(states.tiktok)
        states.tiktok = setInterval(tiktok, 1000);

        hour.innerHTML = states.originalTime.hour;
        min.innerHTML = states.originalTime.min;
        sec.innerHTML = states.originalTime.sec;
    })

    includes.forEach(function (element, index) {
        element.addEventListener('click', function () {
            let time = parseInt(min.innerHTML) + index + 1;
            if (time <= 59) {
                addTime(min, index + 1);
            } else if (parseInt(hour.innerHTML) !== 24) {
                let exceeded = time - 60;
                addTime(hour);
                min.innerHTML = '0' + exceeded;
            } else {
                min.innerHTML = '59'
                sec.innerHTML = '59'
            }
        })
    })
})()