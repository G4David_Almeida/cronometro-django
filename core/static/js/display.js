(function (){
    'use strict'
    const hour = document.querySelector('.display .hours');
    const min = document.querySelector('.display .min');
    const sec = document.querySelector('.display .sec');
    const state = {
        tiktok: null,
        timeStartedIn: null,
        timeWithIncludes: null
    }
    
    // Pusher.logToConsole = true;
    var pusher = new Pusher('1851d205f2c34c7bddd4', { cluster: 'us2' });
    var channel = pusher.subscribe('chronometer');

    function tiktok () {
        let hours = parseInt(hour.innerHTML);
        let minutes = parseInt(min.innerHTML);
        let seconds = parseInt(sec.innerHTML);
        
        if (hours === 0 && minutes === 0 && seconds === 0) {
            clearInterval(state.tiktok);
            alert('Tempo acabou');
        } else {
            if (seconds > 0) {
                removeTime(sec);
            } else if (minutes > 0) {
                removeTime(min);
                sec.innerHTML = 59;
            } else {
                removeTime(hour);
                min.innerHTML = 59;
                sec.innerHTML = 59;
            }
        }
    }

    channel.bind('start', function(data) {
        data = JSON.parse(JSON.stringify(data));

        state.timeStartedIn = data.timeStartedIn;
        state.timeWithIncludes = data.timeWithIncludes;

        if (!state.tiktok) {
            state.tiktok = setInterval(tiktok, 1000);
        }
    });

    channel.bind('pause', function(data) {
        data = JSON.parse(JSON.stringify(data));

        if (state.tiktok) {
            clearInterval(state.tiktok);
            state.tiktok = null;
        }
    });

    channel.bind('continue', function(data) {
        data = JSON.parse(JSON.stringify(data));

        if (!state.tiktok) {
            state.tiktok = setInterval(tiktok, 1000);
        }
    })

    channel.bind('restart', function(data) {
        data = JSON.parse(JSON.stringify(data));

        state.timeStartedIn = data.timeStartedIn
        state.timeWithIncludes = data.timeWithIncludes

        hour.innerHTML = state.timeStartedIn.hours;
        min.innerHTML = state.timeStartedIn.minutes;
        sec.innerHTML = state.timeStartedIn.seconds;

        clearInterval(state.tiktok)
        state.tiktok = null;
        state.tiktok = setInterval(tiktok, 1000);
    })
    
    channel.bind('includes', function (data) {
        data = JSON.parse(JSON.stringify(data));
        
        state.timeStartedIn = data.state.timeStartedIn
        state.timeWithIncludes = data.state.timeWithIncludes

        let time = parseInt(min.innerHTML) + data.plusMinutes;

        if (time <= 59) {
            addTime(min, data.plusMinutes);
        } else if (parseInt(hour.innerHTML) !== 24) {
            let exceeded = time - 60;
            addTime(hour);
            min.innerHTML = '0' + exceeded;
        } else {
            min.innerHTML = '59';
            sec.innerHTML = '59';
        }
    })

    function removeTime (element) {
        let time = parseInt(element.innerHTML) - 1;
        if (time < 10) {   
            element.innerHTML = '0' + time;
        } else {
            element.innerHTML = time;
        }
    }

    function addTime (element, value=false) {
        let time = value
            ? parseInt(element.innerHTML) + value
            : parseInt(element.innerHTML) + 1;

        if (time < 10) {   
            element.innerHTML = '0' + time;
        } else {
            element.innerHTML = time;
        }
    }
})()