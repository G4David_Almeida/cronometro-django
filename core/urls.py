from django.urls import path
from .views import IndexView, DisplaView, ControlsView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('controls/', ControlsView.as_view(), name='controls'),
    path('display/', DisplaView.as_view(), name='display'),
]